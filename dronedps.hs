--import IO

import Network.HTTP
import Control.Applicative
import Network.Curl

import qualified Data.ByteString.Lazy as L

data DamageType = EM | Therm | Kin | Exp

--data HP = Float
--data Resist = Float

data Resists = Shield Float Float Float Float | Armor Float Float Float Float | Hull Bool

data Ship = Ship String Float Resists Float Resists Float Resists

data Damage = Damage DamageType Float

data Drone = Drone String Damage

acolyte = Drone "Acolyte"  $ Damage EM 32
hobgoblin = Drone "Hobgoblin" $ Damage Therm 36.5
hornet = Drone "Hornet" $ Damage Kin 34.2
warrior = Drone "Warrior" $ Damage Exp 29.7

drones = [acolyte, hobgoblin, hornet, warrior]

hullAlgos = Ship "Hull Algos" 830 (Shield 12.5 30 47.5 56.2) 999 (Armor 57.5 44.8 44.8 23.5) 1190 $ Hull True


emResist (Shield r _ _ _) = r
emResist (Armor r _ _ _) = r
emResist (Hull r)
    | r == True = 60
    | otherwise = 0

thermResist (Shield _ r _ _) = r
thermResist (Armor _ r _ _) = r
thermResist (Hull r)
    | r == True = 60
    | otherwise = 0

kinResist (Shield _ _ r _) = r
kinResist (Armor _ _ r _) = r
kinResist (Hull r)
    | r == True = 60
    | otherwise = 0

expResist (Shield _ _ _ r) = r
expResist (Armor _ _ _ r) = r
expResist (Hull r)
    | r == True = 60
    | otherwise = 0


-- time to die
ttd (Damage typ dps) hp resist = hp * ((100 - r) / 100) / dps
                                 where r = case typ of
                                             EM -> emResist resist
                                             Therm -> thermResist resist
                                             Kin -> kinResist resist
                                             Exp -> expResist resist


shipTTD (Ship _ s sr a ar h hr) (Drone _ dps) = (ttd dps s sr) + (ttd dps a ar) + (ttd dps h hr)

shipName (Ship n _ _ _ _ _ _) = n
droneName (Drone n _) = n

main = do
  mapM (putStrLn) $ map (summarize hullAlgos) drones

summarize h d = shipName h ++ " takes " ++ show (shipTTD h d) ++ "s to die to " ++ droneName d
