{-# LANGUAGE DeriveGeneric #-}

import Network.Curl
import GHC.Generics

import Control.Applicative
import Control.Monad
import Data.Aeson
import Network.Curl.Aeson

data Resists = Resists { em :: Float
                       , thermal :: Float
                       , kinetic :: Float
                       , explosive :: Float
                       }
               deriving (Generic, Show)

data Tank = Tank { capacity :: Float
                 , resonance :: Resists
                 }
            deriving (Generic, Show)

data EaR = EaR { shield :: Tank
                 , armor :: Tank
                 , hull :: Tank
                 }
            deriving (Generic, Show)

data Ship = Ship { ehpAndResonances :: EaR }
            deriving (Generic, Show)

data Wrapper = Wrapper { ship :: Ship }
               deriving (Generic, Show)

instance FromJSON Resists
instance FromJSON Tank
instance FromJSON EaR
instance FromJSON Ship
instance FromJSON Wrapper

ticker :: IO Wrapper
ticker = curlAesonGet "https://o.smium.org/api/json/loadout/1/attributes/loc:ship,a:ehpAndResonances"

--main = do
--  let html = curlGetString "http://www.haskell.org/" []
--  fmap (putStrLn.show) html    -- this is WRONG, no output
--  putStrLn =<< fmap (show) html -- this works

main = do
  --let html = curlGetString "https://o.smium.org/api/json/loadout/1/attributes/loc:ship,a:ehpAndResonances?minify=0" []
  --putStrLn =<< fmap (show) html
  putStrLn =<< fmap (show) ticker
