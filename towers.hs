{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Network.Curl
import GHC.Generics

import Control.Applicative
import Control.Monad
import Data.Aeson
import Network.Curl.Aeson

data Price = Price {max :: Float
                   , min :: Float
                   }
             deriving (Generic, Show)

data ItemPrice = ItemPrice {buy :: Price
                           , sell :: Price
                           }
                 deriving (Generic, Show)

data ItemPrices = ItemPrices [ItemPrice]
                deriving (Generic, Show)

firstPrice (ItemPrices a) = head a

instance FromJSON Price
instance FromJSON ItemPrice
instance FromJSON ItemPrices

priceItem :: Int -> IO ItemPrices
priceItem itm = curlAesonGet $"http://api.eve-central.com/api/marketstat/json?typeid="++(show itm)++"&usesystem=30000142"


data Item = Item { name :: String
                 , idnum :: Int
                 }



vanadium = Item {name = "Vanadium"
                , idnum = 16642}


pricefetch :: Item -> IO ItemPrices
pricefetch itm = priceItem $ idnum itm

raw :: [Item]
raw = [vanadium]

-- [{"Cobalt",16640},{"Scandium",16639},{"Titanium",16638},{"Tungsten",16637},{"Vanadium",16642},]

main = do
  --putStrLn =<< fmap (show) $ map pricefetch raw
  putStrLn =<< fmap (show.firstPrice)  (pricefetch vanadium)
