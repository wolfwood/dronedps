-- The same as above, but we define our own data type which is an
-- instance of FromJSON:
--
{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative
import Control.Monad
import Data.Aeson
import Network.Curl.Aeson

data Ticker = Ticker { bid :: Double
                     , ask :: Double
                     } deriving (Show)

instance FromJSON Ticker where
    parseJSON (Object o) = Ticker <$> o .: "bid" <*> o .: "ask"
    parseJSON _ = mzero

ticker :: IO Ticker
ticker = curlAesonGet "https://paymium.com/api/v1/data/eur/ticker"

main = do
  let req = ticker
  putStrLn =<< fmap (show) req
