#!/usr/bin/env python

droneNames = ["Acolyte", "Hobgoblin", "Hornet", "Warrior"]
droneDps = [32, 36.5, 34.2, 29.7]


resistNames = ["EM", "Therm", "Kin", "Exp"]

algosShieldResist = [12.5, 30 , 47.5, 56.2]
algosArmorResist = [57.5, 44.8, 44.8, 23.5]

def main():
    print "Algos takes ->"
    for d,drone in enumerate(droneNames):
        out = str(droneDps[d] * (100-algosShieldResist[d])/100) + " Shield DPS and " + str(droneDps[d] * (100-algosArmorResist[d])/100) + " Armor DPS from " + drone
        print out


if __name__ == "__main__":
    main()
