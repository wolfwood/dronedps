{-# LANGUAGE OverloadedStrings #-}
import Control.Monad
import Data.Aeson
import Network.Curl.Aeson

ticker :: IO (Double,Double)
ticker = curlAesonGetWith p "https://paymium.com/api/v1/data/eur/ticker"
    where
      p (Object o) = do
        bid <- o .: "bid"
        ask <- o .: "ask"
        return (bid,ask)
      p _ = mzero

main = do
  let req = ticker
  fmap (putStrLn . show) req
